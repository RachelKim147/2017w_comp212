﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SortedLinkedListLibrary;

namespace LinkedListDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            TestMerge();
            
            Console.ReadLine();
        }

        static void TestMerge()
        {
            // Create first linked list.
            SortedLinkedList firstLinkedList = new SortedLinkedList("First linked list");

            firstLinkedList.SortedInsert(0);
            firstLinkedList.Display();

            firstLinkedList.SortedInsert(3);
            firstLinkedList.Display();

            firstLinkedList.SortedInsert(1);
            firstLinkedList.Display();

            firstLinkedList.SortedInsert(5);
            firstLinkedList.Display();

            firstLinkedList.SortedInsert(2);
            firstLinkedList.Display();

            // Create second linked list.
            SortedLinkedList secondLinkedList = new SortedLinkedList("Second linked list");
            secondLinkedList.SortedInsert(100);
            secondLinkedList.Display();

            secondLinkedList.SortedInsert(-1);
            secondLinkedList.Display();

            secondLinkedList.SortedInsert(6);
            secondLinkedList.Display();

            secondLinkedList.SortedInsert(5);
            secondLinkedList.Display();

            secondLinkedList.SortedInsert(7);
            secondLinkedList.Display();

            // Merge two linked list.
            Console.WriteLine("Merging............................");

            SortedLinkedList resultLinkedList = Merge(firstLinkedList, secondLinkedList);
            firstLinkedList.Display();
            secondLinkedList.Display();
            resultLinkedList.Display();
        }

        static SortedLinkedList Merge(SortedLinkedList target1, SortedLinkedList target2)
        {
            // Create new linked list for saving merged data.
            SortedLinkedList mergedList = new SortedLinkedList("Result linked list");

            // Copy all data of target1 to result linked list.
            foreach (Node node in target1)
            {
                mergedList.SortedInsert(node.Data);
            }

            // Copy all data of target2 to result linked list.
            foreach (Node node in target2)
            {
                mergedList.SortedInsert(node.Data);
            }

            // Return the reference of result linked list.
            return mergedList;
        }
    }
}
