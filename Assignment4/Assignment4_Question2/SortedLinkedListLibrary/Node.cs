﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedLinkedListLibrary
{
    public class Node
    {
        // Varables
        public int Data { get; private set; }
        public Node Next { get; set; }

        // Constructor
        public Node(int data) : this(data, null) { }

        // Constructor
        public Node(int data, Node next)
        {
            Data = data;
            Next = next;
        }
    }
}
