﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedLinkedListLibrary
{
    class SortedLinkedListEmptyException :Exception
    {
        public SortedLinkedListEmptyException() : base("The list is empty") { }

        // one-parameter constructor
        public SortedLinkedListEmptyException(string name): base($"The {name} is empty"){ }

        // two-parameter constructor
        public SortedLinkedListEmptyException(string exception, Exception inner): base(exception, inner) { }
    }
}
