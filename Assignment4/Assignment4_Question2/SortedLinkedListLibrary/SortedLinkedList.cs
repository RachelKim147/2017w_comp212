﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortedLinkedListLibrary
{
    public class SortedLinkedList : IEnumerable
    {
        // Member variables
        private string name;
        public Node First { get; private set; }
        public Node Last;

        // Constructor
        public SortedLinkedList() : this("List") { }

        // Constructor
        public SortedLinkedList(string listName)
        {
            name = listName;
        }

        // Add data at front
        public void AddAtFront(int data)
        {
            if (IsEmpty())
            {
                First = new Node(data);
                Last = First;
            }
            else
            {
                First = new Node(data, First);
            }
        }

        // Add data at back
        public void AddAtLast(int data)
        {
            if (IsEmpty())
            {
                First = new Node(data);
                Last = First;
            }
            else
            {
                Node newData = new Node(data);
                Last.Next = newData;
                Last = newData;
            }
        }

        // Add day by ascending order.
        public void SortedInsert(int data)
        {
            if (IsEmpty())
            {
                AddAtFront(data);
            }
            else if(First.Next == null)
            {
                if(First.Data > data)
                {
                    AddAtFront(data);
                }
                else
                {
                    AddAtLast(data);
                }
            }
            else
            {
                Node current = First;

                while (current.Next != null && current.Next.Data < data)
                {
                    current = current.Next;
                }

                Node newData = new Node(data);
                newData.Next = current.Next;
                current.Next = newData;
            }
            
        }

        // Remove node from front, and return the data of deleted node.
        public int RemoveFromFront()
        {
            if (IsEmpty())
            {
                throw new SortedLinkedListEmptyException();
            }

            int returnData = First.Data;
            if(First == Last) // only one
            {
                First = null;
                Last = null;
            }
            else
            {
                First = First.Next;
            }
           
            return returnData;
        }

        // Remove node from back, and return the data of deleted node.
        public int RemoveFromBack()
        {
            if (IsEmpty())
            {
                throw new SortedLinkedListEmptyException();
            }

            int returnData = Last.Data;
            if (First == Last) // only one
            {
                First = null;
                Last = null;
            }
            else
            {
                Node current = First;
                while(current.Next != Last)
                {
                    current = current.Next;
                }

                Last = current;
                Last.Next = null;
            }

            return returnData;
        }

        // Check if liked list is empty or not.
        public bool IsEmpty()
        {
            return First == null;
        }

        // Display all contents of linked list.
        public void Display()
        {
            if (IsEmpty())
            {
                Console.WriteLine($"Empty {name}");
            }
            else
            {
                Console.Write($"The {name} is: ");

                Node current = First;

                // output current node data while not at end of list
                while (current != null)
                {
                    Console.Write($"{current.Data} ");
                    current = current.Next;
                }

                Console.WriteLine("\n");
            }
        }

        //IEnumerator - For "Foreach" statement.
        IEnumerator IEnumerable.GetEnumerator()
        {
            Node current = First;
            while (current != null)
            {             
                yield return current;
                current = current.Next;
            }           
        }
    }
}
