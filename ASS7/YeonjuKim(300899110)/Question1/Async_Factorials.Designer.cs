﻿namespace Question1
{
    partial class Async_Factorials
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_async = new System.Windows.Forms.GroupBox();
            this.label_async_result = new System.Windows.Forms.Label();
            this.button_async_calculate = new System.Windows.Forms.Button();
            this.label_async_title = new System.Windows.Forms.Label();
            this.textBox_async_input = new System.Windows.Forms.TextBox();
            this.groupBox_sync = new System.Windows.Forms.GroupBox();
            this.label_sync_result = new System.Windows.Forms.Label();
            this.button_sync_calculate = new System.Windows.Forms.Button();
            this.groupBox_async.SuspendLayout();
            this.groupBox_sync.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_async
            // 
            this.groupBox_async.Controls.Add(this.label_async_result);
            this.groupBox_async.Controls.Add(this.button_async_calculate);
            this.groupBox_async.Controls.Add(this.label_async_title);
            this.groupBox_async.Controls.Add(this.textBox_async_input);
            this.groupBox_async.Location = new System.Drawing.Point(13, 13);
            this.groupBox_async.Name = "groupBox_async";
            this.groupBox_async.Size = new System.Drawing.Size(454, 81);
            this.groupBox_async.TabIndex = 0;
            this.groupBox_async.TabStop = false;
            this.groupBox_async.Text = "Calculate Asynchoronously";
            // 
            // label_async_result
            // 
            this.label_async_result.AutoSize = true;
            this.label_async_result.Location = new System.Drawing.Point(97, 52);
            this.label_async_result.Name = "label_async_result";
            this.label_async_result.Size = new System.Drawing.Size(0, 13);
            this.label_async_result.TabIndex = 3;
            // 
            // button_async_calculate
            // 
            this.button_async_calculate.Location = new System.Drawing.Point(10, 47);
            this.button_async_calculate.Name = "button_async_calculate";
            this.button_async_calculate.Size = new System.Drawing.Size(75, 23);
            this.button_async_calculate.TabIndex = 2;
            this.button_async_calculate.Text = "Calculate";
            this.button_async_calculate.UseVisualStyleBackColor = true;
            this.button_async_calculate.Click += new System.EventHandler(this.button_async_calculate_Click);
            // 
            // label_async_title
            // 
            this.label_async_title.AutoSize = true;
            this.label_async_title.Location = new System.Drawing.Point(7, 20);
            this.label_async_title.Name = "label_async_title";
            this.label_async_title.Size = new System.Drawing.Size(87, 13);
            this.label_async_title.TabIndex = 1;
            this.label_async_title.Text = "Get Factorials of:";
            // 
            // textBox_async_input
            // 
            this.textBox_async_input.Location = new System.Drawing.Point(100, 19);
            this.textBox_async_input.Name = "textBox_async_input";
            this.textBox_async_input.Size = new System.Drawing.Size(100, 20);
            this.textBox_async_input.TabIndex = 0;
            // 
            // groupBox_sync
            // 
            this.groupBox_sync.Controls.Add(this.label_sync_result);
            this.groupBox_sync.Controls.Add(this.button_sync_calculate);
            this.groupBox_sync.Location = new System.Drawing.Point(13, 100);
            this.groupBox_sync.Name = "groupBox_sync";
            this.groupBox_sync.Size = new System.Drawing.Size(454, 81);
            this.groupBox_sync.TabIndex = 1;
            this.groupBox_sync.TabStop = false;
            this.groupBox_sync.Text = "Calculate Synchoronously";
            // 
            // label_sync_result
            // 
            this.label_sync_result.AutoSize = true;
            this.label_sync_result.Location = new System.Drawing.Point(7, 21);
            this.label_sync_result.Name = "label_sync_result";
            this.label_sync_result.Size = new System.Drawing.Size(37, 13);
            this.label_sync_result.TabIndex = 3;
            this.label_sync_result.Text = "Result";
            // 
            // button_sync_calculate
            // 
            this.button_sync_calculate.Location = new System.Drawing.Point(10, 47);
            this.button_sync_calculate.Name = "button_sync_calculate";
            this.button_sync_calculate.Size = new System.Drawing.Size(94, 23);
            this.button_sync_calculate.TabIndex = 2;
            this.button_sync_calculate.Text = "Next Number";
            this.button_sync_calculate.UseVisualStyleBackColor = true;
            this.button_sync_calculate.Click += new System.EventHandler(this.button_sync_calculate_Click);
            // 
            // Async_Factorials
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 195);
            this.Controls.Add(this.groupBox_sync);
            this.Controls.Add(this.groupBox_async);
            this.Name = "Async_Factorials";
            this.Text = "Factorias Calculator";
            this.groupBox_async.ResumeLayout(false);
            this.groupBox_async.PerformLayout();
            this.groupBox_sync.ResumeLayout(false);
            this.groupBox_sync.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_async;
        private System.Windows.Forms.Button button_async_calculate;
        private System.Windows.Forms.Label label_async_title;
        private System.Windows.Forms.TextBox textBox_async_input;
        private System.Windows.Forms.Label label_async_result;
        private System.Windows.Forms.GroupBox groupBox_sync;
        private System.Windows.Forms.Label label_sync_result;
        private System.Windows.Forms.Button button_sync_calculate;
    }
}