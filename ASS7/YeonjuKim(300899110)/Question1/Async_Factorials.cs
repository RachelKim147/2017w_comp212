﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Question1
{
    public partial class Async_Factorials : Form
    {
        private long num1_sync = 0;
        private long count_sync = 0;

        public Async_Factorials()
        {
            InitializeComponent();
        }

        private async void button_async_calculate_Click(object sender, EventArgs e)
        {
            // retrieve user's input as an integer
            int number = 0;

            if( int.TryParse(textBox_async_input.Text, out number) == false || number < 0 )
            {
                // There is no factorials for negative number. 
                // Also, we only accept number.
                MessageBox.Show("Please put positive number only!", "Error");
                return;
            }
            else if( number > 20 )
            {
                // To prevent overflow, don't aceept number more than 20.
                // long max:  9,223,372,036,854,775,807
                // 21!:      51,090,942,171,709,440,000
                MessageBox.Show("Overflow!\nPlease put positive number under 21!", "Error");
                return;
            }

            label_async_result.Text = "Calculating...";

            // Task to perform Factorials calculation in separate thread
            Task<long> factorialsTask = Task.Run(() => Factorias(number));

            // wait for Task in separate thread to complete
            await factorialsTask;

            // display result after Task in separate thread completes
            label_async_result.Text = factorialsTask.Result.ToString();
        }

        private long Factorias(long n)
        {
            if (n <= 1)
                return 1;

            return n * Factorias(n - 1);
        }

        private void button_sync_calculate_Click(object sender, EventArgs e)
        {
            if (count_sync > 20)
            {
                MessageBox.Show("Overflow!! Stop calculating factorias!", "Error");
                return;
            }

            if (count_sync <= 1)
                num1_sync = 1;
            else
                num1_sync = num1_sync * count_sync;
            
            label_sync_result.Text = $"Result of Factorial calculation for {count_sync}!: {num1_sync}";
            count_sync++;
        }
    }
}
