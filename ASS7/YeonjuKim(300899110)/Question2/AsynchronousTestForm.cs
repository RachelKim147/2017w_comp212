﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Question2
{
    public partial class AsynchronousTestForm : Form
    {
        public AsynchronousTestForm()
        {
            InitializeComponent();
        }

        // start asynchronous calls to Fibonacci
        private async void startButton_Click(object sender, EventArgs e)
        {
            outputTextBox.Text =
               "Starting Task to calculate Fibonacci(46)\r\n";

            // create Task to perform Fibonacci(46) calculation in a thread
            Task<TimeData> task1 = Task.Run(() => StartFibonacci(46));

            outputTextBox.AppendText(
               "Starting Task to calculate Fibonacci(45)\r\n");

            // create Task to perform Fibonacci(45) calculation in a thread
            Task<TimeData> task2 = Task.Run(() => StartFibonacci(45));

            TimeData[] results = await Task.WhenAll(task1, task2); // wait for both to complete

            // @Yeonju Kim {2017/0415} - the results by using the array returned by Task produced by Task method WhenAll. 
            TimeData resultTime = new TimeData();
            if(results.Length == 2) // We have only two tasks.
            {
                if (results[0].StartTime < results[1].StartTime)
                {
                    resultTime.StartTime = results[0].StartTime;
                }
                else
                {
                    resultTime.StartTime = results[1].StartTime;
                }

                if (results[0].EndTime > results[1].EndTime)
                {
                    resultTime.EndTime = results[0].EndTime;
                }
                else
                {
                    resultTime.EndTime = results[1].EndTime;
                }
            }
            else
            {
                MessageBox.Show($"Please check the length of Task -{results.Length}", "Error");
            }
            
            // display total time for calculations
            double r_totalMinutes = (resultTime.EndTime - resultTime.StartTime).TotalMinutes;
            outputTextBox.AppendText($"Total calculation time = {r_totalMinutes:F6} minutes\r\n");
        }

        // starts a call to fibonacci and captures start/end times
        TimeData StartFibonacci(int n)
        {
            // create a TimeData object to store start/end times
            var result = new TimeData();

            AppendText($"Calculating Fibonacci({n})");
            result.StartTime = DateTime.Now;
            long fibonacciValue = Fibonacci(n);
            result.EndTime = DateTime.Now;

            AppendText($"Fibonacci({n}) = {fibonacciValue}");
            double minutes =
               (result.EndTime - result.StartTime).TotalMinutes;
            AppendText($"Calculation time = {minutes:F6} minutes\r\n");

            return result;
        }

        // Recursively calculates Fibonacci numbers
        public long Fibonacci(long n)
        {
            if (n == 0 || n == 1)
            {
                return n;
            }
            else
            {
                return Fibonacci(n - 1) + Fibonacci(n - 2);
            }
        }

        // append text to outputTextBox in UI thread
        public void AppendText(String text)
        {
            if (InvokeRequired) // not GUI thread, so add to GUI thread
            {
                Invoke(new MethodInvoker(() => AppendText(text)));
            }
            else // GUI thread so append text
            {
                outputTextBox.AppendText(text + "\r\n");
            }
        }
    }
}
