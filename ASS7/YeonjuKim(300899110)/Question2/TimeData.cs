﻿// TimeData.cs
// class to store the start and end times of a calculation
using System;

namespace Question2
{
    class TimeData
    {
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
    }
}
