﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Baseball;
using System.Data.Entity;

namespace DisplayBBPlayer
{
    public partial class FormBBPlayer : Form
    {
        // Variables
        private BaseballEntities dbContext = null;
        // Variables End

        // Default constructor
        public FormBBPlayer()
        {
            InitializeComponent();
        }

        // Dispose old dbContext
        private void DisposeDbContext()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }

        // Create new dbContext
        private void CreateDbContext()
        {
            DisposeDbContext();

            dbContext = new BaseballEntities();
        }

        // Display all player information
        private void buttonDisplay_Click(object sender, EventArgs e)
        {
            try
            {
                //bindingNavigatorAddNewItem.Enabled = true;
                //bindingNavigatorDeleteItem.Enabled = true;

                CreateDbContext();

                dbContext.Players.Load();
                playerBindingSource.DataSource = dbContext.Players.Local;
            }
            catch(Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        // Search specific player by last name
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            string lastName = textBoxLastName.Text.Trim();

            // Check if lastName is vaid form.
            if(lastName.Length == 0)
            {
                MessageBox.Show("Please put valid last name.");
            }
            else
            {
                try
                {
                    CreateDbContext();

                    var lastNameQuery = from player in dbContext.Players
                                        where player.LastName.StartsWith(lastName)
                                        select player;

                    playerBindingSource.DataSource = lastNameQuery.ToList();

                    //bindingNavigatorAddNewItem.Enabled = false;
                    //bindingNavigatorDeleteItem.Enabled = false;
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }
    }
}
