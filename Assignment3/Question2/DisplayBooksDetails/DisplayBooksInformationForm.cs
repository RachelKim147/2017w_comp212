﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Books;

namespace DisplayBooksDetails
{
    public partial class DisplayBooksInformationForm : Form
    {
        // Variables
        enum ESearchType
        {
            ESEARCH_NONE,
            ESEARCH_BASIC,
            ESEARCH_SORT_BY_AUTHOR,
            ESEARCH_GROUP_BY_TITLE
        }

        private ESearchType selectedSearchType = ESearchType.ESEARCH_NONE;
        private RadioButton selectedRadioButton = null;

        // Default constructor
        public DisplayBooksInformationForm()
        {
            InitializeComponent();
        }

        private void radioButtonSearch1_CheckedChanged(object sender, EventArgs e)
        {
            selectedRadioButton = sender as RadioButton;
            selectedSearchType = ESearchType.ESEARCH_BASIC;
        }

        private void radioButtonSearch2_CheckedChanged(object sender, EventArgs e)
        {
            selectedRadioButton = sender as RadioButton;
            selectedSearchType = ESearchType.ESEARCH_SORT_BY_AUTHOR;
        }

        private void radioButtonSearch3_CheckedChanged(object sender, EventArgs e)
        {
            selectedRadioButton = sender as RadioButton;
            selectedSearchType = ESearchType.ESEARCH_GROUP_BY_TITLE;
        }

        private void buttonSearch1_Click(object sender, EventArgs e)
        {
            textBoxResult.Text = "";

            switch (selectedSearchType)
            {
                case ESearchType.ESEARCH_BASIC:
                    DisplaySearchingResult_SortByTitle();
                    break;
                case ESearchType.ESEARCH_SORT_BY_AUTHOR:
                    DisplaySearchingResult_SortByTitle_AuthorName();
                    break;
                case ESearchType.ESEARCH_GROUP_BY_TITLE:
                    DisplaySearchingResult_GroupByTitle_SortByTitle_AuthorName();
                    break;
                case ESearchType.ESEARCH_NONE:
                default:
                    textBoxResult.Text = String.Format("Invaild radio button type - {0}", selectedSearchType);
                    MessageBox.Show(textBoxResult.Text);
                    break;
            }          
        }

        // Query1. Get a list of all the titles and the authors who wrote them. Sort the results by title.
        private void DisplaySearchingResult_SortByTitle()
        {
            BooksEntities dbcontext = new BooksEntities();

            var autorsTitles =
                from book in dbcontext.Titles // titles
                from author in book.Authors // authors
                orderby book.Title1 // sort by title
                select new { book.Title1, author.FirstName, author.LastName };

            string title_display = "";
            string author_display = "";
            foreach (var element in autorsTitles)
            {
                title_display = String.Format("{0}\r\n", element.Title1);
                author_display = String.Format("\t{0} {1}\r\n\r\n", element.FirstName, element.LastName);
                textBoxResult.AppendText(title_display + author_display);
            } // end foreach (var title in autorsTitles)
        }

        // Query2. Get a list of all the titles and the authors who wrote them. Sort the results by title.  
        //          Each title sort the authors alphabetically by last name, then first name
        private void DisplaySearchingResult_SortByTitle_AuthorName()
        {
            BooksEntities dbcontext = new BooksEntities();

            var autorsTitles =
                from book in dbcontext.Titles // titles
                from author in book.Authors // authors
                orderby book.Title1, author.LastName, author.FirstName // sort by title, then author's last name and first name
                select new { book.Title1, author.FirstName, author.LastName };

            string title_display = "";
            string author_display = "";
            foreach (var element in autorsTitles)
            {
                title_display = String.Format("{0}\r\n", element.Title1);
                author_display = String.Format("\t{0} {1}\r\n\r\n", element.FirstName, element.LastName);
                textBoxResult.AppendText(title_display + author_display);
            } // end foreach (var title in autorsTitles)
        }

        // Query 3. Get a list of all the authors grouped by title, sorted by title; 
        //          for a given title sort the author names alphabetically by last name then first name.
        private void DisplaySearchingResult_GroupByTitle_SortByTitle_AuthorName()
        {
            // Entity Framework DbContext
            BooksEntities dbcontext = new BooksEntities();

            // Get a list of all the titles and the authors who wrote them. Sort the results by title.
            var autorsTitles =

                from book in dbcontext.Titles // titles

                orderby book.Title1 // sort by title
                select new
                {
                    Title = book.Title1,
                    Authors = from author in book.Authors // authors
                              orderby author.LastName, author.FirstName
                              select author
                };

            string title_display = "";
            string author_display = "";
            foreach (var title in autorsTitles)
            {
                title_display = String.Format("Title: {0}\r\n", title.Title);
                textBoxResult.AppendText(title_display);

                foreach (var author in title.Authors)
                {
                    author_display = String.Format("\t{0} {1}\r\n", author.FirstName, author.LastName);
                    textBoxResult.AppendText(author_display);
                }

                textBoxResult.AppendText("\r\n");
            } // end foreach (var title in autorsTitles)
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            try
            {
                selectedRadioButton.Checked = false;
            }
            catch(Exception error)
            {
                MessageBox.Show(error.Message);
            }

            selectedSearchType = ESearchType.ESEARCH_NONE;
            selectedRadioButton = null;
            textBoxResult.Text = "";
        }
    }
}