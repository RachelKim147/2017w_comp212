﻿namespace DisplayBooksDetails
{
    partial class DisplayBooksInformationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.groupBoxSearch = new System.Windows.Forms.GroupBox();
            this.radioButtonSearch3 = new System.Windows.Forms.RadioButton();
            this.radioButtonSearch2 = new System.Windows.Forms.RadioButton();
            this.radioButtonSearch1 = new System.Windows.Forms.RadioButton();
            this.buttonClear = new System.Windows.Forms.Button();
            this.groupBoxSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxResult
            // 
            this.textBoxResult.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBoxResult.Location = new System.Drawing.Point(17, 213);
            this.textBoxResult.Multiline = true;
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.ReadOnly = true;
            this.textBoxResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxResult.Size = new System.Drawing.Size(674, 203);
            this.textBoxResult.TabIndex = 0;
            this.textBoxResult.TabStop = false;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonSearch.Location = new System.Drawing.Point(17, 167);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(79, 23);
            this.buttonSearch.TabIndex = 4;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch1_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(13, 23);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(223, 20);
            this.labelTitle.TabIndex = 7;
            this.labelTitle.Text = "List of Titles and Authors";
            // 
            // groupBoxSearch
            // 
            this.groupBoxSearch.Controls.Add(this.radioButtonSearch3);
            this.groupBoxSearch.Controls.Add(this.radioButtonSearch2);
            this.groupBoxSearch.Controls.Add(this.radioButtonSearch1);
            this.groupBoxSearch.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBoxSearch.Location = new System.Drawing.Point(17, 60);
            this.groupBoxSearch.Name = "groupBoxSearch";
            this.groupBoxSearch.Size = new System.Drawing.Size(674, 100);
            this.groupBoxSearch.TabIndex = 11;
            this.groupBoxSearch.TabStop = false;
            this.groupBoxSearch.Text = "Search Options";
            // 
            // radioButtonSearch3
            // 
            this.radioButtonSearch3.AutoSize = true;
            this.radioButtonSearch3.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.radioButtonSearch3.Location = new System.Drawing.Point(7, 68);
            this.radioButtonSearch3.Name = "radioButtonSearch3";
            this.radioButtonSearch3.Size = new System.Drawing.Size(654, 22);
            this.radioButtonSearch3.TabIndex = 2;
            this.radioButtonSearch3.TabStop = true;
            this.radioButtonSearch3.Text = "Group by titles and sort by titles, then authors\' name(last name, first name) alp" +
    "habetically";
            this.radioButtonSearch3.UseVisualStyleBackColor = true;
            this.radioButtonSearch3.CheckedChanged += new System.EventHandler(this.radioButtonSearch3_CheckedChanged);
            // 
            // radioButtonSearch2
            // 
            this.radioButtonSearch2.AutoSize = true;
            this.radioButtonSearch2.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.radioButtonSearch2.Location = new System.Drawing.Point(7, 45);
            this.radioButtonSearch2.Name = "radioButtonSearch2";
            this.radioButtonSearch2.Size = new System.Drawing.Size(518, 22);
            this.radioButtonSearch2.TabIndex = 1;
            this.radioButtonSearch2.TabStop = true;
            this.radioButtonSearch2.Text = "Sort by titles, then authors\' name(last name, first name) alphabetically";
            this.radioButtonSearch2.UseVisualStyleBackColor = true;
            this.radioButtonSearch2.CheckedChanged += new System.EventHandler(this.radioButtonSearch2_CheckedChanged);
            // 
            // radioButtonSearch1
            // 
            this.radioButtonSearch1.AutoSize = true;
            this.radioButtonSearch1.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.radioButtonSearch1.Location = new System.Drawing.Point(7, 20);
            this.radioButtonSearch1.Name = "radioButtonSearch1";
            this.radioButtonSearch1.Size = new System.Drawing.Size(214, 22);
            this.radioButtonSearch1.TabIndex = 0;
            this.radioButtonSearch1.TabStop = true;
            this.radioButtonSearch1.Text = "Sort by titles alphabetically";
            this.radioButtonSearch1.UseVisualStyleBackColor = true;
            this.radioButtonSearch1.CheckedChanged += new System.EventHandler(this.radioButtonSearch1_CheckedChanged);
            // 
            // buttonClear
            // 
            this.buttonClear.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonClear.Location = new System.Drawing.Point(102, 167);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(79, 23);
            this.buttonClear.TabIndex = 12;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // DisplayBooksInformationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 433);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.groupBoxSearch);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.textBoxResult);
            this.Name = "DisplayBooksInformationForm";
            this.Text = "Display Books Information Form";
            this.groupBoxSearch.ResumeLayout(false);
            this.groupBoxSearch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.GroupBox groupBoxSearch;
        private System.Windows.Forms.RadioButton radioButtonSearch3;
        private System.Windows.Forms.RadioButton radioButtonSearch2;
        private System.Windows.Forms.RadioButton radioButtonSearch1;
        private System.Windows.Forms.Button buttonClear;
    }
}

