﻿namespace SearchDrive
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_generate = new System.Windows.Forms.Button();
            this.textBox_intArray = new System.Windows.Forms.TextBox();
            this.textBox_doubleArray = new System.Windows.Forms.TextBox();
            this.textBox_search = new System.Windows.Forms.TextBox();
            this.button_search = new System.Windows.Forms.Button();
            this.groupBox_generate = new System.Windows.Forms.GroupBox();
            this.groupBox_search = new System.Windows.Forms.GroupBox();
            this.textBox_result = new System.Windows.Forms.TextBox();
            this.radioButton_int = new System.Windows.Forms.RadioButton();
            this.radioButton_double = new System.Windows.Forms.RadioButton();
            this.label_intArray = new System.Windows.Forms.Label();
            this.label_doubleArray = new System.Windows.Forms.Label();
            this.groupBox_generate.SuspendLayout();
            this.groupBox_search.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_generate
            // 
            this.button_generate.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_generate.Location = new System.Drawing.Point(21, 23);
            this.button_generate.Name = "button_generate";
            this.button_generate.Size = new System.Drawing.Size(96, 27);
            this.button_generate.TabIndex = 0;
            this.button_generate.Text = "Generate";
            this.button_generate.UseVisualStyleBackColor = true;
            this.button_generate.Click += new System.EventHandler(this.button_generate_Click);
            // 
            // textBox_intArray
            // 
            this.textBox_intArray.Location = new System.Drawing.Point(21, 81);
            this.textBox_intArray.Multiline = true;
            this.textBox_intArray.Name = "textBox_intArray";
            this.textBox_intArray.ReadOnly = true;
            this.textBox_intArray.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_intArray.Size = new System.Drawing.Size(100, 257);
            this.textBox_intArray.TabIndex = 1;
            // 
            // textBox_doubleArray
            // 
            this.textBox_doubleArray.Location = new System.Drawing.Point(143, 81);
            this.textBox_doubleArray.Multiline = true;
            this.textBox_doubleArray.Name = "textBox_doubleArray";
            this.textBox_doubleArray.ReadOnly = true;
            this.textBox_doubleArray.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_doubleArray.Size = new System.Drawing.Size(169, 257);
            this.textBox_doubleArray.TabIndex = 2;
            // 
            // textBox_search
            // 
            this.textBox_search.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.textBox_search.Location = new System.Drawing.Point(18, 89);
            this.textBox_search.Name = "textBox_search";
            this.textBox_search.Size = new System.Drawing.Size(100, 24);
            this.textBox_search.TabIndex = 3;
            // 
            // button_search
            // 
            this.button_search.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_search.Location = new System.Drawing.Point(124, 87);
            this.button_search.Name = "button_search";
            this.button_search.Size = new System.Drawing.Size(96, 26);
            this.button_search.TabIndex = 4;
            this.button_search.Text = "Search";
            this.button_search.UseVisualStyleBackColor = true;
            this.button_search.Click += new System.EventHandler(this.button_search_Click);
            // 
            // groupBox_generate
            // 
            this.groupBox_generate.Controls.Add(this.label_doubleArray);
            this.groupBox_generate.Controls.Add(this.label_intArray);
            this.groupBox_generate.Controls.Add(this.button_generate);
            this.groupBox_generate.Controls.Add(this.textBox_intArray);
            this.groupBox_generate.Controls.Add(this.textBox_doubleArray);
            this.groupBox_generate.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox_generate.Location = new System.Drawing.Point(32, 32);
            this.groupBox_generate.Name = "groupBox_generate";
            this.groupBox_generate.Size = new System.Drawing.Size(341, 355);
            this.groupBox_generate.TabIndex = 5;
            this.groupBox_generate.TabStop = false;
            this.groupBox_generate.Text = "Generate arrays";
            // 
            // groupBox_search
            // 
            this.groupBox_search.Controls.Add(this.radioButton_double);
            this.groupBox_search.Controls.Add(this.radioButton_int);
            this.groupBox_search.Controls.Add(this.textBox_result);
            this.groupBox_search.Controls.Add(this.textBox_search);
            this.groupBox_search.Controls.Add(this.button_search);
            this.groupBox_search.Font = new System.Drawing.Font("Meiryo UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox_search.Location = new System.Drawing.Point(402, 32);
            this.groupBox_search.Name = "groupBox_search";
            this.groupBox_search.Size = new System.Drawing.Size(252, 355);
            this.groupBox_search.TabIndex = 6;
            this.groupBox_search.TabStop = false;
            this.groupBox_search.Text = "Search";
            // 
            // textBox_result
            // 
            this.textBox_result.Location = new System.Drawing.Point(18, 127);
            this.textBox_result.Multiline = true;
            this.textBox_result.Name = "textBox_result";
            this.textBox_result.ReadOnly = true;
            this.textBox_result.Size = new System.Drawing.Size(202, 211);
            this.textBox_result.TabIndex = 7;
            // 
            // radioButton_int
            // 
            this.radioButton_int.AutoSize = true;
            this.radioButton_int.Checked = true;
            this.radioButton_int.Location = new System.Drawing.Point(25, 24);
            this.radioButton_int.Name = "radioButton_int";
            this.radioButton_int.Size = new System.Drawing.Size(140, 22);
            this.radioButton_int.TabIndex = 8;
            this.radioButton_int.TabStop = true;
            this.radioButton_int.Text = "Search Int Array";
            this.radioButton_int.UseVisualStyleBackColor = true;
            // 
            // radioButton_double
            // 
            this.radioButton_double.AutoSize = true;
            this.radioButton_double.Location = new System.Drawing.Point(25, 53);
            this.radioButton_double.Name = "radioButton_double";
            this.radioButton_double.Size = new System.Drawing.Size(168, 22);
            this.radioButton_double.TabIndex = 9;
            this.radioButton_double.Text = "Search Double Array";
            this.radioButton_double.UseVisualStyleBackColor = true;
            // 
            // label_intArray
            // 
            this.label_intArray.AutoSize = true;
            this.label_intArray.Location = new System.Drawing.Point(32, 60);
            this.label_intArray.Name = "label_intArray";
            this.label_intArray.Size = new System.Drawing.Size(70, 18);
            this.label_intArray.TabIndex = 3;
            this.label_intArray.Text = "Int Array";
            // 
            // label_doubleArray
            // 
            this.label_doubleArray.AutoSize = true;
            this.label_doubleArray.Location = new System.Drawing.Point(175, 60);
            this.label_doubleArray.Name = "label_doubleArray";
            this.label_doubleArray.Size = new System.Drawing.Size(98, 18);
            this.label_doubleArray.TabIndex = 4;
            this.label_doubleArray.Text = "Double Array";
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 412);
            this.Controls.Add(this.groupBox_search);
            this.Controls.Add(this.groupBox_generate);
            this.Name = "SearchForm";
            this.Text = "Search Form";
            this.groupBox_generate.ResumeLayout(false);
            this.groupBox_generate.PerformLayout();
            this.groupBox_search.ResumeLayout(false);
            this.groupBox_search.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_generate;
        private System.Windows.Forms.TextBox textBox_intArray;
        private System.Windows.Forms.TextBox textBox_doubleArray;
        private System.Windows.Forms.TextBox textBox_search;
        private System.Windows.Forms.Button button_search;
        private System.Windows.Forms.GroupBox groupBox_generate;
        private System.Windows.Forms.GroupBox groupBox_search;
        private System.Windows.Forms.TextBox textBox_result;
        private System.Windows.Forms.RadioButton radioButton_double;
        private System.Windows.Forms.RadioButton radioButton_int;
        private System.Windows.Forms.Label label_doubleArray;
        private System.Windows.Forms.Label label_intArray;
    }
}

