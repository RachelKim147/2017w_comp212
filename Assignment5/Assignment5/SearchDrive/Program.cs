﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SearchDrive
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SearchForm());
        }

        private static int ArrayLinearSearch<T>(T[] data, T searchKey) where T : IComparable<T>
        {
            int count = 0;
            foreach (T item in data)
            {
                if (item.CompareTo(searchKey) == 0)
                {
                    return count;
                }

                count++;
            }
            return -1;
        }
      

        private static int IntArrayLinearSearch(int[] data, int searchKey)
        {
            int length = data.Length;
            for (int i = 0; i < length; i++)
            {
                if (data[i] == searchKey)
                {
                    return i;
                }
            }

            return -1;
        }

        private static int DoubleArrayLinearSearch(double[] data, double searchKey)
        {
            int length = data.Length;
            for (int i = 0; i < length; i++)
            {
                if (data[i] == searchKey)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
