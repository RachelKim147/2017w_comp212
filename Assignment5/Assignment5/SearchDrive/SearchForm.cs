﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SearchDrive
{
    public partial class SearchForm : Form
    {
        // variables
        private int[] intArray;
        private double[] doubleArray;
        private const int MAX_ARRAY = 20;

        public SearchForm()
        {
            InitializeComponent();

            // Init arrays.
            intArray = new int[MAX_ARRAY];
            doubleArray = new double[MAX_ARRAY];
        }

        private void button_generate_Click(object sender, EventArgs e)
        {
            // Give different seed so that two arrays have different values.
            Random intRandom = new Random(10);
            Random doubleRandom = new Random(20);

            for(int i = 0; i < MAX_ARRAY; i++)
            {
                intArray[i] = intRandom.Next(MAX_ARRAY);
                // For convenience of searching, only the third decimal place is used.
                doubleArray[i] = Math.Round(doubleRandom.NextDouble() * MAX_ARRAY, 3);
            }

            // Display two arrays
            textBox_intArray.Text = "";
            foreach(int data in intArray)
            {
                textBox_intArray.AppendText($"{data}\n");
            }

            textBox_doubleArray.Text = "";
            foreach (double data in doubleArray)
            {
                textBox_doubleArray.AppendText($"{data}\n");
            }
        }

        private void button_search_Click(object sender, EventArgs e)
        {
            int intNum = 0;
            double doubleNum = 0.0;
            int index = 0;

            textBox_result.Text = "";

            // Check if the input value is empty or not.
            string inputNumber = textBox_search.Text.Trim();
            if(inputNumber.Length == 0 || inputNumber == "")
            {
                textBox_result.Text = "Search input is empty.";
                return;
            }

            // Converto to input value(string) to int or double.
            if (radioButton_int.Checked)
            {
                if (Int32.TryParse(inputNumber, out intNum) == false)
                {
                    textBox_result.Text = "Please put only number.";
                    return;
                }

                index = ArrayLinearSearch<int>(intArray, intNum);
            }
            else if (radioButton_double.Checked)
            {
                if (Double.TryParse(inputNumber, out doubleNum) == false)
                {
                    textBox_result.Text = "Please put only number.";
                    return;
                }

                index = ArrayLinearSearch<double>(doubleArray, doubleNum);
            }

            // Display result.
            if (index >= 0)
            {
                textBox_result.Text = $"The number, {inputNumber}, is in the list. Index is {index}.";
            }
            else
            {
                textBox_result.Text = $"The number, {inputNumber}, is not founded.";
            }           
        }

        // Generic Search Method
        private static int ArrayLinearSearch<T>(T[] data, T searchKey) where T : IComparable<T>
        {
            int count = 0;
            foreach (T item in data)
            {
                if (item.CompareTo(searchKey) == 0)
                {
                    return count;
                }

                count++;
            }
            return -1;
        }
    }
}
