﻿// Fig. 12.9: PayrollSystemTest.cs
// Employee hierarchy test app.
using System;
using System.Collections.Generic;
using System.Linq;

class PayrollSystemTest
{
    static void Main()
    {
        // @Yeonju Kim {2017/04/07} - Create 10 employees information.
        List<Employee> baseList = new List<Employee>();
        // Three Salaried Employee information
        baseList.Add(new SalariedEmployee("Phung", "Bergquist", "111-11-1111", 800.00M));
        baseList.Add(new SalariedEmployee("Micah", "Weingarten", "111-11-1112", 900.00M));
        baseList.Add(new SalariedEmployee("Noella", "Litwin", "111-11-1113", 650.00M));
        // Two Hourly Employee information
        baseList.Add(new HourlyEmployee("Waldo", "Lago", "222-222-1111", 16.75M, 40.0M));
        baseList.Add(new HourlyEmployee("Suzy", "Freeburg", "222-222-1112", 21.48M, 80.0M));
        // Three Base Plus Commission Employee information
        baseList.Add(new BasePlusCommissionEmployee("Nellie", "Weigel", "333-33-1111", 5000.00M, .04M, 300.00M));
        baseList.Add(new BasePlusCommissionEmployee("Phyliss", "Nantz", "333-33-1112", 6000.00M, .05M, 400.00M));
        baseList.Add(new BasePlusCommissionEmployee("Chae", "Crampton", "333-33-1113", 7000.00M, .06M, 500.00M));
        // Two Commission Employee information
        baseList.Add(new CommissionEmployee("Sherika", "Baldwin", "444-44-1111", 10000.00M, .06M));
        baseList.Add(new CommissionEmployee("Tenesha", "Schapiro", "444-44-1112", 20500.00M, .02M));

        Console.WriteLine("Employees processed polymorphically:\n");
        
        foreach(Employee empl in baseList)
        {
            Console.WriteLine($"Name: {empl.FirstName} {empl.LastName} / Earning: ${empl.Earnings()}");
        }

        // @Yeonju Kim {2017/04/07}
        // Update the PayrollSytem by useing LINQ to map the List<Employee> 
        // into a list of anonymous objects in which each object contains an Employee’s name and earning. 

        // @Name : full name
        // @Earning : double type because we will multiply 1.1 to base-salary
        var summaryList = baseList.Where((Employee empl) => !(empl is BasePlusCommissionEmployee))
                                  .Select((Employee empl) => new {
                                        Name = $"{empl.FirstName} {empl.LastName}",
                                        Earning = (double)empl.Earnings()
                                   })
                                  .ToList();

        // When a BasePlusCommissionEmployee is encountered, 
        // give a 10% base-salary increase without modifying the original BasePlusCommissionEmployee object.
        // @Name : full name
        // @Earning : double type because we will multiply 1.1 to base-salary
        summaryList.AddRange(baseList.Where((Employee empl) => empl is BasePlusCommissionEmployee)
                                  .Select((Employee empl) => new {
                                      Name = $"{empl.FirstName} {empl.LastName}",
                                      Earning = ((BasePlusCommissionEmployee)empl).Earnings(0.1)
                                  })
                                  .ToList());

        summaryList.AddRange(baseList.Where(empl => empl is BasePlusCommissionEmployee)
                                  .Select(empl => new {
                                      Name = $"{empl.FirstName} {empl.LastName}",
                                      Earning = ((BasePlusCommissionEmployee)empl).Earnings(0.1)
                                  })
                                  .ToList());

        Console.WriteLine("\nDisplay Final Earnings Information:\n");

        // @Yeonju Kim {2017/04/08} - Display the names and earnings.
        foreach (var empl in summaryList)
        {
            Console.WriteLine($"Name:{empl.Name} / Earnings: ${empl.Earning}");
        }

        Console.ReadLine();
    }
}

// @Yeonju Kim {2017/04/07} - declare extension methods
static class Extensions
{
    // @Yeonju Kim {2017/04/08} - the extension method that calculate 
    public static double Earnings(this BasePlusCommissionEmployee empl, double increasingRate)
    {
        return (double)empl.BaseSalary * increasingRate + (double)empl.Earnings();
    }
}


/**************************************************************************
 * (C) Copyright 1992-2017 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 **************************************************************************/
