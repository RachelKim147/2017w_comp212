﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SortedLinkedListClassLibrary;

namespace ReverseDrive
{
    public static class MyExtensions
    {
        // Second version of Extension.
        // First version is in SortedLinkedList.cs.
        public static void Reverse_Extension(this SortedLinkedList list)
        {
            if (list.IsEmpty())
            {
                throw new EmptySortedLinkListException();
            }
            else
            {
                ListNode previous = null;
                ListNode current = list.FirstNode;
                ListNode next = null;

                while (current != null)
                {
                    // save next node
                    next = current.Next;

                    // Change the linkage: from "pre->cur" to "cur->pre"
                    current.Next = previous;

                    // For next step, save current node as previous
                    previous = current;
                    // For next step, save next node as current
                    current = next;
                }

                list.LastNode = list.FirstNode;
                list.FirstNode = previous;
            }
        }
    }
}
