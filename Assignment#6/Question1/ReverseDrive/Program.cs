﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SortedLinkedListClassLibrary;

namespace ReverseDrive
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedLinkedList testList = new SortedLinkedList();

            testList.Insert(999);
            testList.Insert(0);
            testList.Insert(100);
            testList.Insert(103);
            testList.Insert(-1);

            Console.WriteLine("Before reverse:");
            testList.Display();

            Console.WriteLine("After reverse:");
            testList.Reverse();
            testList.Display();

            SortedLinkedList testList1 = new SortedLinkedList();

            testList1.Insert(0);
            testList1.Insert(5);
            testList1.Insert(2);
            testList1.Insert(-1);

            Console.WriteLine("Before reverse:");
            testList1.Display();

            Console.WriteLine("After reverse:");
            testList1.Reverse_Extension();
            testList1.Display();

            Console.ReadLine();

        }
    }
}
