﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YeonjuKim_300899110_
{
    public partial class NotificationForm : Form
    {
        Publisher _publisher;

        public NotificationForm()
        {
            InitializeComponent();
            _publisher = new Publisher();
        }

        private void btnPublish_Click(object sender, EventArgs e)
        {
            try
            {
                _publisher.PublishMessage(textNotificationMsg.Text);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
