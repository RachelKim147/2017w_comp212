﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YeonjuKim_300899110_
{
    class Publisher
    {
        // The list of email and phone number for notification.
        private static Dictionary<string, SendViaEmail> _hmEmail;
        private static Dictionary<string, SendViaPhone> _hmPhone;

        // Delegate
        public delegate void PublisherDel(string message);
        public static PublisherDel publishMsg;

        // Static constructor to initialize the static variable.
        // It is invoked before the first instance constructor is run.
        static Publisher()
        {
            _hmEmail = new Dictionary<string, SendViaEmail>(StringComparer.CurrentCultureIgnoreCase);
            _hmPhone = new Dictionary<string, SendViaPhone>(StringComparer.CurrentCultureIgnoreCase);
            publishMsg = null;
        }

        public void PublishMessage(string message)
        {
            // Notify that there is no contact information if both lists are empty.
            if (_hmEmail.Count == 0 && _hmPhone.Count == 0)
            {
                MessageBox.Show("There is no connact information for notification!", "Notification");
            }
            else
            {
                try
                {
                    publishMsg.Invoke(message);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
        
        // List control methods-----------------------------------------
        public bool IsNewEmail(string email)
        {
            return !_hmEmail.ContainsKey(email);
        }

        public void AddEmail(string emailKey, SendViaEmail emailValue)
        {
            _hmEmail.Add(emailKey, emailValue);
        }

        public SendViaEmail GetEmail(string emailKey)
        {
            SendViaEmail out_email = new SendViaEmail();

            if (!_hmEmail.TryGetValue(emailKey, out out_email))
                out_email = null;

            return out_email;
        }

        public void RemoveEmail(string emailKey)
        {
            _hmEmail.Remove(emailKey);
        }

        public bool IsNewPhone(string phone)
        {
            return !_hmPhone.ContainsKey(phone);
        }

        public void AddPhone(string phoneKey, SendViaPhone phoneValue)
        {
            _hmPhone.Add(phoneKey, phoneValue);
        }

        public SendViaPhone GetPhone(string phoneKey)
        {
            SendViaPhone out_phone = new SendViaPhone();

            if (!_hmPhone.TryGetValue(phoneKey, out out_phone))
                out_phone = null;

            return out_phone;
        }

        public void RemovePhone(string phoneKey)
        {
            _hmPhone.Remove(phoneKey);
        }
        // End List control methods-----------------------------------------
    }
}