﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace YeonjuKim_300899110_
{
    class Utilites
    {
        // References: How to: Verify that Strings Are in Valid Email Format
        // https://msdn.microsoft.com/en-ca/library/01escwtf(v=vs.110).aspx
        bool invalidEmail = false;

        // References: How to: Verify that Strings Are in Valid Email Format
        // https://msdn.microsoft.com/en-ca/library/01escwtf(v=vs.110).aspx
        public bool IsValidEmail(string strIn)
        {
            invalidEmail = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalidEmail)
                return false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        // References: How to: Verify that Strings Are in Valid Email Format
        // https://msdn.microsoft.com/en-ca/library/01escwtf(v=vs.110).aspx
        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalidEmail = true;
            }
            return match.Groups[1].Value + domainName;
        } 

        public bool IsVaildMobile(string strIn)
        {
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Return true if strIn is in valid mobile format.
            try
            {
                return Regex.IsMatch(strIn, @"\d{3}-\d{3}-\d{4}");
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
