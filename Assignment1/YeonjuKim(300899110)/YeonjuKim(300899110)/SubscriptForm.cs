﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YeonjuKim_300899110_
{
    public partial class SubscriptForm : Form
    {
        Publisher _publisher;

        public SubscriptForm()
        {
            InitializeComponent();
            _publisher = new Publisher();
        }

        private void btnSubscribe_Click(object sender, EventArgs e)
        {
            if(cbSentEmail.Checked)
            {
                Utilites checkEmail = new Utilites();
                if( checkEmail.IsValidEmail(textEmail.Text) )
                {
                    SendViaEmail newMail = new SendViaEmail(textEmail.Text);
                    newMail.TrySubscript(_publisher);
                }
                else
                {
                    MessageBox.Show(string.Format("This is not email form. - {0}", textEmail.Text));
                }
            }

            if (cbSentMobile.Checked)
            {
                Utilites checkMobile = new Utilites();
                if (checkMobile.IsVaildMobile(textPhone.Text))
                {
                    SendViaPhone newPhone = new SendViaPhone(textPhone.Text);
                    newPhone.TrySubscript(_publisher);
                }
                else
                {
                    MessageBox.Show(string.Format("This is not mobile form. - {0}", textPhone.Text));
                }
            }
        }

        private void btnUnsubscribe_Click(object sender, EventArgs e)
        {
            if (cbSentEmail.Checked)
            {
                SendViaEmail email = _publisher.GetEmail(textEmail.Text);
                if (email != null)
                {
                    email.TryUnsubscript(_publisher);
                }
                else
                {
                    MessageBox.Show(String.Format("Not existed email - {0}", textEmail.Text));
                }
            }

            if (cbSentMobile.Checked)
            {
                SendViaPhone phone = _publisher.GetPhone(textPhone.Text);
                if (phone != null)
                {
                    phone.TryUnsubscript(_publisher);
                }
                else
                {
                    MessageBox.Show(String.Format("Not existed phone - {0}", textPhone.Text));
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
