﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YeonjuKim_300899110_
{
    class SendViaEmail
    {
        private string _emailAddress;
        public SendViaEmail()
        {

        }

        public SendViaEmail(string email)
        {
            _emailAddress = email;
        }

        public void sendEmail(string msg)
        {
            MessageBox.Show(String.Format("{0} : {1}", _emailAddress, msg), "Email");
        }

        public void TrySubscript(Publisher pub)
        {
            // Add email address if it is new one.
            if (pub.IsNewEmail(_emailAddress))
            {
                pub.AddEmail(_emailAddress, this);
                Subscript(pub);
                MessageBox.Show(String.Format("Subscript new email - {0}", _emailAddress));
            }
            else // Otherwise, show the error message.
            {
                MessageBox.Show(String.Format("Already existed email - {0}", _emailAddress));
            }
        }

        private void Subscript(Publisher pub)
        {
            Publisher.publishMsg += sendEmail;
        }

        public void TryUnsubscript(Publisher pub)
        {
            // Remove email address if it exists.
            if (!pub.IsNewEmail(_emailAddress))
            {
                Unsubscript(pub);
                pub.RemoveEmail(_emailAddress);
                MessageBox.Show(String.Format("Unsubscript email - {0}", _emailAddress));
            }
            else // Otherwise, show the error message.
            {
                MessageBox.Show(String.Format("Not existed email - {0}", _emailAddress));
            }
        }
        private void Unsubscript(Publisher pub)
        {
            Publisher.publishMsg -= sendEmail;
        }
    }
}
