﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YeonjuKim_300899110_
{
    class SendViaPhone
    {
        private string _phoneNumber;

        public SendViaPhone()
        {

        }

        public SendViaPhone(string phoneNumber)
        {
            _phoneNumber = phoneNumber;
        }

        public void SendText(string msg)
        {
            MessageBox.Show(String.Format("{0} : {1}", _phoneNumber, msg), "Mobile");
        }

        public void TrySubscript(Publisher pub)
        {
            // Add phone number if it is new one.
            if (pub.IsNewPhone(_phoneNumber))
            {
                pub.AddPhone(_phoneNumber, this);
                Subscript(pub);
                MessageBox.Show(String.Format("Added new phone - {0}", _phoneNumber));
            }
            else // Otherwise, show the error message.
            {
                MessageBox.Show(String.Format("Already existed phone - {0}", _phoneNumber));
            }
        }

        private void Subscript(Publisher pub)
        {
            Publisher.publishMsg += SendText;
        }

        public void TryUnsubscript(Publisher pub)
        {
            // Remove phone number if it exists.
            if (!pub.IsNewPhone(_phoneNumber))
            {
                Unsubscript(pub);
                pub.RemovePhone(_phoneNumber);
                MessageBox.Show(String.Format("Unsubscript phone - {0}", _phoneNumber));
            }
            else // Otherwise, show the error message.
            {
                MessageBox.Show(String.Format("Not existed phone - {0}", _phoneNumber));
            }
        }
        private void Unsubscript(Publisher pub)
        {
            Publisher.publishMsg -= SendText;
        }
    }
}
