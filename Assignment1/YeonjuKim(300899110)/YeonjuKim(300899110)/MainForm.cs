﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YeonjuKim_300899110_
{
    public partial class MainForm : Form
    {
        private SubscriptForm subscriptForm;
        private NotificationForm notificationFrom;
        // Constructor
        public MainForm()
        {
            InitializeComponent();
            subscriptForm = new SubscriptForm();
            notificationFrom = new NotificationForm();
        }

        private void btnManage_Click(object sender, EventArgs e)
        {
            subscriptForm.Show();
        }

        private void btnPublish_Click(object sender, EventArgs e)
        {
            notificationFrom.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
