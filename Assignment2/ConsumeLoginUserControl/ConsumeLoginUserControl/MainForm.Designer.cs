﻿namespace ConsumeLoginUserControl
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginUserControl = new LoginUserControl.LoginUserControl();
            this.button_submit = new System.Windows.Forms.Button();
            this.label_result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // loginUserControl
            // 
            this.loginUserControl.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.loginUserControl.Font = new System.Drawing.Font("Century Gothic", 20.25F);
            this.loginUserControl.Location = new System.Drawing.Point(32, 34);
            this.loginUserControl.Margin = new System.Windows.Forms.Padding(8);
            this.loginUserControl.Name = "loginUserControl";
            this.loginUserControl.Size = new System.Drawing.Size(625, 219);
            this.loginUserControl.TabIndex = 0;
            // 
            // button_submit
            // 
            this.button_submit.Location = new System.Drawing.Point(509, 264);
            this.button_submit.Name = "button_submit";
            this.button_submit.Size = new System.Drawing.Size(148, 36);
            this.button_submit.TabIndex = 1;
            this.button_submit.Text = "Submit";
            this.button_submit.UseVisualStyleBackColor = true;
            this.button_submit.Click += new System.EventHandler(this.button_submit_Click);
            // 
            // label_result
            // 
            this.label_result.AutoSize = true;
            this.label_result.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_result.Location = new System.Drawing.Point(157, 341);
            this.label_result.Name = "label_result";
            this.label_result.Size = new System.Drawing.Size(0, 24);
            this.label_result.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 432);
            this.Controls.Add(this.label_result);
            this.Controls.Add(this.button_submit);
            this.Controls.Add(this.loginUserControl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LoginUserControl.LoginUserControl loginUserControl;
        private System.Windows.Forms.Button button_submit;
        private System.Windows.Forms.Label label_result;
    }
}

