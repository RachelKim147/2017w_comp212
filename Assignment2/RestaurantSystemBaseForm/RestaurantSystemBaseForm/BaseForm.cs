﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RestaurantSystemBaseForm
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();
        }

        private void toolStripSplitButton_goWebsite_ButtonClick(object sender, EventArgs e)
        {
            Process.Start("IEXPLORE.EXE", "http://en.tomntoms.com/html/eng/");
        }
    }
}
