﻿namespace RestaurantBillCalculator_
{
    partial class BillCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_clear = new System.Windows.Forms.Button();
            this.label_tax = new System.Windows.Forms.Label();
            this.label_total = new System.Windows.Forms.Label();
            this.label_resultTax = new System.Windows.Forms.Label();
            this.label_resultTotal = new System.Windows.Forms.Label();
            this.comboBox_beverage = new System.Windows.Forms.ComboBox();
            this.comboBox_appetizer = new System.Windows.Forms.ComboBox();
            this.comboBox_mainCourse = new System.Windows.Forms.ComboBox();
            this.comboBox_dessert = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_selectedMenulist = new System.Windows.Forms.TextBox();
            this.label_subtotal = new System.Windows.Forms.Label();
            this.label_resultSubtotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_clear
            // 
            this.button_clear.Location = new System.Drawing.Point(720, 18);
            this.button_clear.Margin = new System.Windows.Forms.Padding(4);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(112, 30);
            this.button_clear.TabIndex = 0;
            this.button_clear.Text = "Clear";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // label_tax
            // 
            this.label_tax.AutoSize = true;
            this.label_tax.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_tax.Location = new System.Drawing.Point(520, 429);
            this.label_tax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_tax.Name = "label_tax";
            this.label_tax.Size = new System.Drawing.Size(37, 17);
            this.label_tax.TabIndex = 2;
            this.label_tax.Text = "Tax:";
            // 
            // label_total
            // 
            this.label_total.AutoSize = true;
            this.label_total.Location = new System.Drawing.Point(520, 460);
            this.label_total.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_total.Name = "label_total";
            this.label_total.Size = new System.Drawing.Size(47, 17);
            this.label_total.TabIndex = 3;
            this.label_total.Text = "Total:";
            // 
            // label_resultTax
            // 
            this.label_resultTax.AutoSize = true;
            this.label_resultTax.Location = new System.Drawing.Point(608, 429);
            this.label_resultTax.Name = "label_resultTax";
            this.label_resultTax.Size = new System.Drawing.Size(58, 17);
            this.label_resultTax.TabIndex = 4;
            this.label_resultTax.Text = "$ 0.00";
            // 
            // label_resultTotal
            // 
            this.label_resultTotal.AutoSize = true;
            this.label_resultTotal.Location = new System.Drawing.Point(608, 460);
            this.label_resultTotal.Name = "label_resultTotal";
            this.label_resultTotal.Size = new System.Drawing.Size(58, 17);
            this.label_resultTotal.TabIndex = 5;
            this.label_resultTotal.Text = "$ 0.00";
            // 
            // comboBox_beverage
            // 
            this.comboBox_beverage.FormattingEnabled = true;
            this.comboBox_beverage.Location = new System.Drawing.Point(137, 18);
            this.comboBox_beverage.Name = "comboBox_beverage";
            this.comboBox_beverage.Size = new System.Drawing.Size(212, 25);
            this.comboBox_beverage.TabIndex = 6;
            this.comboBox_beverage.SelectedIndexChanged += new System.EventHandler(this.comboBox_beverage_SelectedIndexChanged);
            // 
            // comboBox_appetizer
            // 
            this.comboBox_appetizer.FormattingEnabled = true;
            this.comboBox_appetizer.Location = new System.Drawing.Point(137, 65);
            this.comboBox_appetizer.Name = "comboBox_appetizer";
            this.comboBox_appetizer.Size = new System.Drawing.Size(212, 25);
            this.comboBox_appetizer.TabIndex = 7;
            this.comboBox_appetizer.SelectedIndexChanged += new System.EventHandler(this.comboBox_appetizer_SelectedIndexChanged);
            // 
            // comboBox_mainCourse
            // 
            this.comboBox_mainCourse.FormattingEnabled = true;
            this.comboBox_mainCourse.Location = new System.Drawing.Point(137, 114);
            this.comboBox_mainCourse.Name = "comboBox_mainCourse";
            this.comboBox_mainCourse.Size = new System.Drawing.Size(212, 25);
            this.comboBox_mainCourse.TabIndex = 8;
            this.comboBox_mainCourse.SelectedIndexChanged += new System.EventHandler(this.comboBox_mainCourse_SelectedIndexChanged);
            // 
            // comboBox_dessert
            // 
            this.comboBox_dessert.FormattingEnabled = true;
            this.comboBox_dessert.Location = new System.Drawing.Point(137, 165);
            this.comboBox_dessert.Name = "comboBox_dessert";
            this.comboBox_dessert.Size = new System.Drawing.Size(212, 25);
            this.comboBox_dessert.TabIndex = 9;
            this.comboBox_dessert.SelectedIndexChanged += new System.EventHandler(this.comboBox_dessert_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Beverage";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Appetizer";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Main Course";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Dessert";
            // 
            // textBox_selectedMenulist
            // 
            this.textBox_selectedMenulist.Location = new System.Drawing.Point(385, 15);
            this.textBox_selectedMenulist.Multiline = true;
            this.textBox_selectedMenulist.Name = "textBox_selectedMenulist";
            this.textBox_selectedMenulist.ReadOnly = true;
            this.textBox_selectedMenulist.Size = new System.Drawing.Size(293, 363);
            this.textBox_selectedMenulist.TabIndex = 14;
            // 
            // label_subtotal
            // 
            this.label_subtotal.AutoSize = true;
            this.label_subtotal.Location = new System.Drawing.Point(520, 400);
            this.label_subtotal.Name = "label_subtotal";
            this.label_subtotal.Size = new System.Drawing.Size(71, 17);
            this.label_subtotal.TabIndex = 15;
            this.label_subtotal.Text = "Subtotal:";
            // 
            // label_resultSubtotal
            // 
            this.label_resultSubtotal.AutoSize = true;
            this.label_resultSubtotal.Location = new System.Drawing.Point(608, 400);
            this.label_resultSubtotal.Name = "label_resultSubtotal";
            this.label_resultSubtotal.Size = new System.Drawing.Size(58, 17);
            this.label_resultSubtotal.TabIndex = 16;
            this.label_resultSubtotal.Text = "$ 0.00";
            // 
            // BillCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 530);
            this.Controls.Add(this.label_resultSubtotal);
            this.Controls.Add(this.label_subtotal);
            this.Controls.Add(this.textBox_selectedMenulist);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox_dessert);
            this.Controls.Add(this.comboBox_mainCourse);
            this.Controls.Add(this.comboBox_appetizer);
            this.Controls.Add(this.comboBox_beverage);
            this.Controls.Add(this.label_resultTotal);
            this.Controls.Add(this.label_resultTax);
            this.Controls.Add(this.label_total);
            this.Controls.Add(this.label_tax);
            this.Controls.Add(this.button_clear);
            this.Font = new System.Drawing.Font("Britannic Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "BillCalculator";
            this.Text = "Bill Calculator";
            this.Load += new System.EventHandler(this.BillCalculator_Load);
            this.Controls.SetChildIndex(this.button_clear, 0);
            this.Controls.SetChildIndex(this.label_tax, 0);
            this.Controls.SetChildIndex(this.label_total, 0);
            this.Controls.SetChildIndex(this.label_resultTax, 0);
            this.Controls.SetChildIndex(this.label_resultTotal, 0);
            this.Controls.SetChildIndex(this.comboBox_beverage, 0);
            this.Controls.SetChildIndex(this.comboBox_appetizer, 0);
            this.Controls.SetChildIndex(this.comboBox_mainCourse, 0);
            this.Controls.SetChildIndex(this.comboBox_dessert, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.textBox_selectedMenulist, 0);
            this.Controls.SetChildIndex(this.label_subtotal, 0);
            this.Controls.SetChildIndex(this.label_resultSubtotal, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.Label label_tax;
        private System.Windows.Forms.Label label_total;
        private System.Windows.Forms.Label label_resultTax;
        private System.Windows.Forms.Label label_resultTotal;
        private System.Windows.Forms.ComboBox comboBox_beverage;
        private System.Windows.Forms.ComboBox comboBox_appetizer;
        private System.Windows.Forms.ComboBox comboBox_mainCourse;
        private System.Windows.Forms.ComboBox comboBox_dessert;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_selectedMenulist;
        private System.Windows.Forms.Label label_subtotal;
        private System.Windows.Forms.Label label_resultSubtotal;
    }
}

