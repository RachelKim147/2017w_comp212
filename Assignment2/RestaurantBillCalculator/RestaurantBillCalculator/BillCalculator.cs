﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestaurantSystemBaseForm;

namespace RestaurantBillCalculator_
{
    public partial class BillCalculator : BaseForm
    {
        // For combobox data
        private List<Menu> Beverages;
        private List<Menu> Appetizers;
        private List<Menu> MainCourses;
        private List<Menu> Desserts;

        // For calculating bill
        private double subtotal;
        private double tax;
        private double total;

        public BillCalculator()
        {
            InitializeComponent();
            ClearValues();
            InitMenus();           
        }

        // Set comboboxes
        private void InitMenus()
        {
            InitMenus_Beverages();
            InitMenus_Appetizers();
            InitMenus_MainCourses();
            InitMenus_Desserts();
        }

        private void ClearValues()
        {
            textBox_selectedMenulist.Clear();

            label_resultSubtotal.Text = "$ 0.00";
            label_resultTax.Text = "$ 0.00";
            label_resultTotal.Text = "$ 0.00";

            subtotal = 0;
            tax = 0;
            total = 0;
        }

        private void BillCalculator_Load(object sender, EventArgs e)
        {
            // [TODO] Because of SelectedIndexChanged() calling, we need to clear everything again. 
            ClearValues();
        }

        private void button_clear_Click(object sender, EventArgs e)
        {
            ClearValues();
        }

        private void comboBox_beverage_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox_selectedMenulist.Text += comboBox_beverage.Text + "  $" + comboBox_beverage.SelectedValue + Environment.NewLine;

            AddSubtotal(comboBox_beverage.SelectedValue.ToString());
            CalculateTax();
            CalculateTotal();

            DisplayAll();
        }

        private void comboBox_appetizer_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox_selectedMenulist.Text += comboBox_appetizer.Text + "  $" + comboBox_appetizer.SelectedValue + Environment.NewLine;

            AddSubtotal(comboBox_appetizer.SelectedValue.ToString());
            CalculateTax();
            CalculateTotal();

            DisplayAll();
        }

        private void comboBox_mainCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox_selectedMenulist.Text += comboBox_mainCourse.Text + "  $" + comboBox_mainCourse.SelectedValue + Environment.NewLine;

            AddSubtotal(comboBox_mainCourse.SelectedValue.ToString());
            CalculateTax();
            CalculateTotal();

            DisplayAll();
        }

        private void comboBox_dessert_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox_selectedMenulist.Text += comboBox_dessert.Text + "  $" + comboBox_dessert.SelectedValue + Environment.NewLine;

            AddSubtotal(comboBox_dessert.SelectedValue.ToString());
            CalculateTax();
            CalculateTotal();

            DisplayAll();
        }

        private void AddSubtotal(string priceString)
        {
            double selectedPrice = 0;

            double.TryParse(priceString, out selectedPrice);
            subtotal += selectedPrice;
        }

        private void CalculateTax()
        {
            tax = Math.Round(subtotal * 0.13, 2);
        }

        private void CalculateTotal()
        {
            total = subtotal + tax;
        }

        private void DisplayAll()
        {
            label_resultSubtotal.Text = String.Format("$ {0}", subtotal);
            label_resultTax.Text = String.Format("$ {0}", tax);
            label_resultTotal.Text = String.Format("$ {0}", total);
        }

        private void InitMenus_Beverages()
        {
            Beverages = new List<Menu>();

            Menu newItem = new Menu();
            newItem.Name = "Soda";
            newItem.Price = 1.95;
            Beverages.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Tea";
            newItem.Price = 1.50;
            Beverages.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Coffe";
            newItem.Price = 1.25;
            Beverages.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Mineral Warter";
            newItem.Price = 2.95;
            Beverages.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Juice";
            newItem.Price = 2.50;
            Beverages.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Milk";
            newItem.Price = 1.50;
            Beverages.Add(newItem);

            comboBox_beverage.DataSource = new BindingSource(Beverages, null);
            comboBox_beverage.DisplayMember = "Name";
            comboBox_beverage.ValueMember = "Price";
        }

        private void InitMenus_Appetizers()
        {
            Appetizers = new List<Menu>();

            Menu newItem = new Menu();
            newItem.Name = "Nachos";
            newItem.Price = 8.95;
            Appetizers.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Mushroom Caps";
            newItem.Price = 10.95;
            Appetizers.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Shrimp Cocktail";
            newItem.Price = 12.95;
            Appetizers.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Chips and Salsa";
            newItem.Price = 6.95;
            Appetizers.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Seafood Alfredo";
            newItem.Price = 15.95;
            Appetizers.Add(newItem);

            comboBox_appetizer.DataSource = new BindingSource(Appetizers, null);
            comboBox_appetizer.DisplayMember = "Name";
            comboBox_appetizer.ValueMember = "Price";
        }

        private void InitMenus_MainCourses()
        {
            MainCourses = new List<Menu>();

            Menu newItem = new Menu();
            newItem.Name = "Seafood Alfredo";
            newItem.Price = 15.95;
            MainCourses.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Chicken Alfredo";
            newItem.Price = 13.95;
            MainCourses.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Chicken Picatta";
            newItem.Price = 13.95;
            MainCourses.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Turkey Club";
            newItem.Price = 11.95;
            MainCourses.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Lobster Pie";
            newItem.Price = 19.95;
            MainCourses.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Prime Rib";
            newItem.Price = 20.95;
            MainCourses.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Shrimp Scampi";
            newItem.Price = 18.95;
            MainCourses.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Turkey Dinner";
            newItem.Price = 13.95;
            MainCourses.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Stuffed Chicken";
            newItem.Price = 14.95;
            MainCourses.Add(newItem);

            comboBox_mainCourse.DataSource = new BindingSource(MainCourses, null);
            comboBox_mainCourse.DisplayMember = "Name";
            comboBox_mainCourse.ValueMember = "Price";
        }

        private void InitMenus_Desserts()
        {
            Desserts = new List<Menu>();

            Menu newItem = new Menu();
            newItem.Name = "Apple Pie";
            newItem.Price = 5.95;
            Desserts.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Sundae";
            newItem.Price = 3.95;
            Desserts.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Carrot Cake";
            newItem.Price = 5.95;
            Desserts.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Mud Pie";
            newItem.Price = 4.95;
            Desserts.Add(newItem);

            newItem = new Menu();
            newItem.Name = "Apple Crisp";
            newItem.Price = 5.95;
            Desserts.Add(newItem);

            comboBox_dessert.DataSource = new BindingSource(Desserts, null);
            comboBox_dessert.DisplayMember = "Name";
            comboBox_dessert.ValueMember = "Price";
        }
    }
}
